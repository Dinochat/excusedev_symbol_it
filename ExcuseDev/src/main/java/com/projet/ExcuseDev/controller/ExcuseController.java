package com.projet.ExcuseDev.controller;

import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import com.projet.ExcuseDev.bean.ExcuseDevBean;
import com.projet.ExcuseDev.serviceImpl.ExcuseDevServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ExcuseController {

	@Autowired
	private ExcuseDevServiceImpl excuseDevService;

	private List<ExcuseDevBean> listExcuses;

	/**
	 * Page index avec génération aléatoire du message
	 * 
	 * @param model
	 */
	@RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
	public void index(Model model) {
		listExcuses = excuseDevService.listExcuseDev();
		ExcuseDevBean excuse = getRandomExcuse();
		model.addAttribute("excuse", excuse.getMessage());
	}

	/**
	 * Route vers page "Lost"
	 * 
	 * @return
	 */
	@GetMapping(value = { "/lost" })
	public String lostPage() {
		return "lost";
	}
	
	/**
	 * Route vers page "Lost"
	 * 
	 * @return
	 */
	@GetMapping(value = { "/*" })
	public String errorPage() {
		return "error";
	}

	/**
	 * Route pour afficher le message du http_code passé dans l'url
	 * 
	 * @param model
	 * @param http_code
	 * @return
	 */
	@GetMapping(value = { "/ExcuseDev/{http_code}" })
	public String findByHttpCode(Model model, @PathVariable String http_code) {
		ExcuseDevBean excuseByHttp = excuseDevService.excuseByHttpCode(http_code);
		model.addAttribute("excuse", excuseByHttp.getMessage());

		return "httpCode";
	}

	/**
	 * Route qui liste les excuses
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/listeExcuses")
	public String listeExcuses(Model model) {
		listExcuses = excuseDevService.listExcuseDev();
		model.addAttribute("excuses", listExcuses);

		return "listeExcuses";
	}

	/**
	 * Envoie vers la page de formulare pour ajouter une nouvelle excuse
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/add-excuse")
	public String addExcuse(Model model) {
		model.addAttribute("newExcuse", new ExcuseDevBean());
		return "addExcuse";
	}

	/**
	 * Ajoute l'excuse du formulaire après validation, dans la base de données
	 * 
	 * @param newExcuse
	 * @param bindingResult
	 * @return
	 */
	@PostMapping(value = "/add-excuse")
	public String processAddExcuse(@Valid @ModelAttribute("newExcuse") ExcuseDevBean newExcuse,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "index";
		}
		excuseDevService.addExcuse(newExcuse);
		return "index";
	}

	/**
	 * Selection d'une excuse aleatoire dans la liste
	 * 
	 * @return ExcuseDevBean
	 */
	public ExcuseDevBean getRandomExcuse() {
		Random rand = new Random();
		int index = rand.nextInt(listExcuses.size());

		ExcuseDevBean excuse = listExcuses.get(index);
		listExcuses.remove(index);

		return excuse;
	}
}
