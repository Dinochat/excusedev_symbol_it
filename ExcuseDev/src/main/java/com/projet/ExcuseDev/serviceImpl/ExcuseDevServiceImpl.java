package com.projet.ExcuseDev.serviceImpl;

import java.util.Collection;
import java.util.List;

import com.projet.ExcuseDev.Repository.IExcuseDevRepository;
import com.projet.ExcuseDev.bean.ExcuseDevBean;
import com.projet.ExcuseDev.daoImpl.ExcuseDevDaoImpl;
import com.projet.ExcuseDev.service.ExcuseDevService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExcuseDevServiceImpl implements ExcuseDevService {
	
	@Autowired
	private ExcuseDevDaoImpl excuseDevDao;
	
	@Autowired
	private IExcuseDevRepository excuseRepository;
	
	@Override
	public List<ExcuseDevBean> listExcuseDev(){
		return excuseDevDao.listExcuseDev();
	}

	@Override
	public void addExcuse(ExcuseDevBean excuseDev) {
		excuseRepository.save(excuseDev);
	}

	@Override
	public ExcuseDevBean excuseByHttpCode(String httpCode) {
		return excuseDevDao.excuseByHttpCode(httpCode);
	}
}
