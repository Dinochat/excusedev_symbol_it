package com.projet.ExcuseDev.dao;

import java.util.Collection;
import java.util.List;

import com.projet.ExcuseDev.bean.ExcuseDevBean;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExcuseDevDao {

	public List<ExcuseDevBean> listExcuseDev();
	
	public void update(ExcuseDevBean excuseDev);

    public ExcuseDevBean add(ExcuseDevBean excuseDev);

    public void delete(ExcuseDevBean excuseDev);
    
    public ExcuseDevBean excuseByHttpCode(String httpCode);
    
}
