package com.projet.ExcuseDev.Repository;

import java.util.List;

import com.projet.ExcuseDev.bean.ExcuseDevBean;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IExcuseDevRepository extends JpaRepository < ExcuseDevBean, Long > {
	
}

