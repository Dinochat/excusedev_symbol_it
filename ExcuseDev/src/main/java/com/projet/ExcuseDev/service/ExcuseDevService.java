package com.projet.ExcuseDev.service;

import java.util.Collection;
import java.util.List;

import com.projet.ExcuseDev.bean.ExcuseDevBean;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExcuseDevService {

	public List<ExcuseDevBean> listExcuseDev();

	public void addExcuse(ExcuseDevBean excuseDev);

	public ExcuseDevBean excuseByHttpCode(String httpCode);
}
