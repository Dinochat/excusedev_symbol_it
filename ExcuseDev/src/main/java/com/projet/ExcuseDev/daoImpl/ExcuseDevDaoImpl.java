package com.projet.ExcuseDev.daoImpl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.projet.ExcuseDev.bean.ExcuseDevBean;
import com.projet.ExcuseDev.dao.ExcuseDevDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ExcuseDevDaoImpl implements ExcuseDevDao {
	
	@Autowired
	EntityManager em;
	
	@Override
	public List<ExcuseDevBean> listExcuseDev() {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ExcuseDevBean> criteriaQuery = cb.createQuery(ExcuseDevBean.class);
		
		Root<ExcuseDevBean> excuseDevRoot = criteriaQuery.from(ExcuseDevBean.class);
		
		List<ExcuseDevBean> listExcuseDev = em.createQuery(criteriaQuery).getResultList();
		
		return listExcuseDev;
	}

	@Override
	public void update(ExcuseDevBean excuseDev) {
		em.merge(excuseDev);
	}

	@Override
	public ExcuseDevBean add(ExcuseDevBean excuseDev) {
		excuseDev = em.merge(excuseDev);
	    return excuseDev;
	}

	@Override
	public void delete(ExcuseDevBean excuseDev) {
		em.remove(excuseDev);
	}
	
	@Override
	public ExcuseDevBean excuseByHttpCode(String httpCode) {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ExcuseDevBean> criteriaQuery = cb.createQuery(ExcuseDevBean.class);
		
		Root<ExcuseDevBean> excuseDevRoot = criteriaQuery.from(ExcuseDevBean.class);
		
		criteriaQuery.where(cb.equal(excuseDevRoot.get("http_code"), httpCode));
		
		ExcuseDevBean excuseHttpCode = em.createQuery(criteriaQuery).getSingleResult();
		
		return excuseHttpCode;
	}

}
