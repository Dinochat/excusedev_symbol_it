package com.projet.ExcuseDev.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="db_excusedev")
public class ExcuseDevBean {

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="http_code")
	private String http_code;
	
	@Column(name="tag")
	private String tag;
	
	@Column(name="message")
	private String message;
	
	public ExcuseDevBean() {
		super();
	}

	public ExcuseDevBean(String http_code, String tag, String message) {
		super();
		this.http_code = http_code;
		this.tag = tag;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHttp_code() {
		return http_code;
	}

	public void setHttp_code(String http_code) {
		this.http_code = http_code;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
