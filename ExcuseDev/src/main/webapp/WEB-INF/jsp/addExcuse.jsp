<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="http://code.ionicframework.com/1.0.0/css/ionic.css" />
<script src="http://code.ionicframework.com/1.0.0/js/ionic.bundle.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
<script src="http://code.jquery.com/jquery-2.0.1.min.js"></script>
<script
	src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<meta charset="ISO-8859-1">
<title>Ajouter une excuse</title>
</head>
<body>

	<c:url var="actionUrl" value="/add-excuse" />

	<div class="ui-field-contain">
		<form:form action="${actionUrl}" modelAttribute="newExcuse"
			method="POST" acceptCharset="UTF-8">

			<label for="text-1">Http code :</label>
			<form:input path="http_code" />
			<form:errors path="http_code" />
			<form:errors path="http_code" />

			<label for="text-1">Tag :</label>
			<form:input path="tag" />
			<form:errors path="tag" />

			<label for="text-1">Message :</label>
			<form:input path="message" />
			<form:errors path="message" />


			<form:button id="save">Ajouter</form:button>
		</form:form>
	</div>


</body>
</html>
