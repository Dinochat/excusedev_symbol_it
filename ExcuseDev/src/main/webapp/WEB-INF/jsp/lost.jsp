<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lost page</title>
</head>
<body>
	<div align="center">
		<h1> I'm lost </h1>
		<img alt="lostGif" src="https://c.tenor.com/5R1whvx7pOkAAAAC/lost-johntravolta.gif">
	</div>
	
</body>
</html>

<script>
         setTimeout(function(){
            window.location.href = '/index';
         }, 5000);
</script>