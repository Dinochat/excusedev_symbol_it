<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
<script src="http://code.jquery.com/jquery-2.0.1.min.js"></script>
<script
	src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<meta charset="ISO-8859-1">
<title>Excuse par http code</title>
</head>
<body>
	<div data-role="page" data-dialog="true" id="dialog-1">
		<div data-role="header">
			<h1>Message</h1>
		</div>
		<div data-role="content">
			<p style="text-align: center">${excuse}</p>
		</div>
	</div>
</body>
</html>