<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
<script src="http://code.jquery.com/jquery-2.0.1.min.js"></script>
<script
	src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<meta charset="ISO-8859-1">
<title>Liste des excuses</title>
</head>
<body>

	<table data-role="table" id="tabListeExcuses" data-mode="columntoggle"
		class="ui-responsive table-stripe">
		<thead>
			<tr>
				<th>Http_code</th>
				<th data-priority="1">Tag</th>
				<th data-priority="2">Message</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${excuses}" var="excuse">
				<tr>
					<td>${excuse.http_code}</td>
					<td>${excuse.tag}</td>
					<td>${excuse.message}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>