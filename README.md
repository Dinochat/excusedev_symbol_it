# ExcuseDev_Symbol_IT
Test technique Symbol IT

## Getting started

## Name
ExcuseDev

## Description
Test technique pour Symbol IT sur Java et Spring

## Installation du projet
Côté éclipse : 
Ouvrir éclipse
Importer le projet (dossier ExcuseDev du git)
Clique droit sur projet dans "Project Explorer"
Run as > Maven install  (doit être en "BUILD SUCCESS")

Cliquer dans l'onglet : "Help" > "Marketplace" > installer Spring Tool 4 si ce n'est pas déjà installer
Et si besoin : hibernate Tool et DBeaver 

Côté base de données : 
Importer le fichier ***excusedev.sql*** dans PhpMyAdmin 
Puis lancer WAMP pour permettre la connection à la base

si l'erreur suivante apparait lors du Maven install : 
Caused by: org.hibernate.HibernateException: Access to DialectResolutionInfo cannot be null when 'hibernate.dialect'
C'est que WAMP ou MAMP (pour Mac) n'est pas lancer.

Dans le fichier ***application.properties*** modifier les lignes suivantes : 
spring.datasource.url=jdbc:mysql://localhost:3306/excusedev => **(remplacer, si besoin ou si autre OS que windows, par l'url correspondante)**
spring.datasource.username=root => **(mettre le nom d'utilisateur correspondant)**
spring.datasource.password= ..... **(à rajouter si il y a un mot de passe pour PhpMyAdmin)**

Optionel : DBeaver dans éclipse, se connecter à la base de données via les identifiants de PhpMyAdmin



## S'il y a un problème, ne pas hésitez à me contacter

## Lancer le projet
Une fois l'installer terminée, lancer ***Boot dashboard***
Ouvrir un navigateur internet (fireforx ou chrome)
Aller sur ce lien :  http://localhost:8080/index

Pour les différentes pages de l'application : 
http://localhost:8080/index
http://localhost:8080/lost 
http://localhost:8080/ExcuseDev/ {http_code à rajouter ici} par exemple : http://localhost:8080/ExcuseDev/701
http://localhost:8080/add-excuse - permet d'ajouter une nouvelle excuse
http://localhost:8080/listeExcuses - permet de voir la liste des excuses
http://localhost:8080/* 

## S'il y a un problème, ne pas hésitez à me contacter

src/main/webapp/WEB-INF/jsp - toutes les JSP 

## Technologies & logiciel
Java - JDK 14
Spring boot - 2.7
JPA & JSP
Base de données MySQL
PhpMyAdmin et WAMP 

Développer sous éclipse

## Authors
DE JESUS Pauline

